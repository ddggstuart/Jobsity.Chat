# Jobsity.Chat

This is simple Chat. It only allows one chat room and only text is allowed.

# How to run it
## Software required
* node.js
* npm
* Visual Studio 2019
* Net Core Framework 3.1
* dotnet-cli
## Steps
* Clone this repository in your local machine using the following command `git clone https://gitlab.com/ddggstuart/Jobsity.Chat.git`
* In the folder Jobsity.Chat you will find two solutions `Jobsity.Chat.Bot.sln` and `Jobsity.Chat.sln` open each one in a different instance of Visual Studio
* In the solution `Jobsity.Chat` update the appsettings.json connection string with the Sql Server credentials you are going to use.
* Once you have configured the connection string, open the nuget console and execute the folloing commands 
	* `dotnet tool install --global dotnet-ef`
	* `dotnet ef database update --project Jobsity.Chat`
* Execute both solutions
* In a console window, go to de path `Jobsity.Chat\Jobsity.Chat.UI` and execute the folloing commands
	* `npm install -g @angular/cli`
	* `npm install`
	* `ng serve`
* Open a browser window with the URL provided by the last command.
* Register your seft in the appliction by clicking the link register.
* Thanks!!