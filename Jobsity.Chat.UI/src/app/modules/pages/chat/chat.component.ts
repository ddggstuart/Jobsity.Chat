import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ChatService } from 'src/app/core/services/chat.service';
import { LoginService } from 'src/app/core/services/login.service';
import { SignalRService } from 'src/app/core/services/signalr.service';
import { UserMessageModel } from 'src/app/shared/models/user-message.model';

@Component({
  selector: 'chat-page',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatPageComponent {
  userMessages: Array<UserMessageModel> = new Array<UserMessageModel>();
  currentMessage: string = "";

  constructor(private loginService: LoginService, private router: Router, private signalRService: SignalRService, private chatService: ChatService) {
    this.loginService.ValidateToken()
      .subscribe(() => {},
      () => {
        this.router.navigate(["/login"])
      });
    this.signalRService.messageCallback = this.receiveMessage.bind(this);
    this.getChatHistory();
  }

  getChatHistory() {
    this.chatService.getChats()
      .subscribe((data) => {
        if (data.body != null)
          this.userMessages = data.body;
      },(errorResponse) => {
        console.error(errorResponse.error);
      });
  }

  sendMessage() {
    this.signalRService.sendMessage(this.currentMessage);
    this.currentMessage = "";
  }

  private receiveMessage(message: UserMessageModel) {
    this.userMessages.push(message);
  }
}
