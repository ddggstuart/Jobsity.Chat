import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UsersManager } from 'src/app/core/managers/users.manager';
import { LoginService } from 'src/app/core/services/login.service';
import { LoginModel } from 'src/app/shared/models/login.model';

@Component({
  selector: 'login-page',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginPageComponent {
  constructor(private loginService: LoginService, private usersManager: UsersManager, private router: Router){
  }

  loginData: LoginModel = new LoginModel();
  errors: Array<string> = new Array<string>();

  login() {
    this.loginService.login(this.loginData)
      .subscribe((data) => {
        this.usersManager.saveToken(data.body?.jsonWebToken);
        this.router.navigate(["/"])
      },(errorResponse) => {
        this.errors = errorResponse.error.errors;
      });
  }
}
