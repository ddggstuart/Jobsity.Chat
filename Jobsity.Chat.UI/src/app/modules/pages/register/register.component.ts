import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UsersManager } from 'src/app/core/managers/users.manager';
import { RegisterService } from 'src/app/core/services/register.service';
import { LoginModel } from 'src/app/shared/models/login.model';

@Component({
  selector: 'register-page',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterPageComponent {
constructor(private registerService: RegisterService, private usersManager: UsersManager, private router: Router){
}
  loginData: LoginModel = new LoginModel();
  errors: Array<string> = new Array<string>();

  register() {
    this.registerService.register(this.loginData)
      .subscribe((data) => {
        this.usersManager.saveToken(data.body?.jsonWebToken);
        this.router.navigate(["/"])
      },(errorResponse) => {
        this.errors = errorResponse.error.errors;
      });
  }
}
