import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { LoginPageComponent } from './modules/pages/login/login.component';
import { LoginService } from './core/services/login.service';
import { UsersManager } from './core/managers/users.manager';
import { ChatPageComponent } from './modules/pages/chat/chat.component';
import { ChatHttpClient } from './core/services/chat-http-client';
import { RegisterService } from './core/services/register.service';
import { RegisterPageComponent } from './modules/pages/register/register.component';
import { SignalRService } from './core/services/signalr.service';
import { ChatService } from './core/services/chat.service';

@NgModule({
  declarations: [
    AppComponent,
    LoginPageComponent,
    ChatPageComponent,
    RegisterPageComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    NgbModule,
    FormsModule
  ],
  providers: [
    LoginService,
    UsersManager,
    ChatHttpClient,
    RegisterService,
    SignalRService,
    ChatService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
