export class UserModel {
    username?: string;
    jsonWebToken?: string;
    errors?: any;
}