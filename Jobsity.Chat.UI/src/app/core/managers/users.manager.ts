import { Injectable } from '@angular/core';
import { LoginService } from '../services/login.service';

const TOKEN_KEY = 'jwt';
const TOKEN_TYPE = 'Bearer ';

@Injectable()
export class UsersManager {
  saveToken(token?: string) {
    if (token)
      localStorage.setItem(TOKEN_KEY, token);
  }

  getToken(includeTokenType: boolean = true) {
    const token = localStorage.getItem(TOKEN_KEY);
    if (token)
    {
      if (includeTokenType)
        return `${TOKEN_TYPE} ${token}`;
      else
        return token;
    }
    return "";
  }
}