import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UsersManager } from '../managers/users.manager';

@Injectable()
export class ChatHttpClient {
  constructor(private http: HttpClient, private usersManager: UsersManager) { }

  post<T>(url: string, body: any | null, options: {
    headers?: HttpHeaders | {
        [header: string]: string | string[];
    };
    observe: 'response';
    params?: HttpParams | {
        [param: string]: string | string[];
    };
    reportProgress?: boolean;
    responseType?: 'json';
    withCredentials?: boolean;
  }): Observable<HttpResponse<T>> {
    options.headers = new HttpHeaders({
        "Authorization": this.usersManager.getToken()
    });
    return this.http.post<T>(url, body, options);
  }

  get<T>(url: string, options: {
    headers?: HttpHeaders | {
        [header: string]: string | string[];
    };
    observe: 'response';
    params?: HttpParams | {
        [param: string]: string | string[];
    };
    reportProgress?: boolean;
    responseType?: 'json';
    withCredentials?: boolean;
  }): Observable<HttpResponse<T>> {
    options.headers = new HttpHeaders({
      'Authorization': this.usersManager.getToken()
    });    
    return this.http.get<T>(url, options);
  } 
}