import { Injectable } from '@angular/core';
import { LOGIN_URL, VALIDATE_TOKEN_URL } from './services.constants'
import { LoginModel } from '../../shared/models/login.model'
import { UserModel } from 'src/app/shared/models/user.model';
import { ChatHttpClient } from './chat-http-client';

@Injectable()
export class LoginService {
  constructor(private http: ChatHttpClient) { }

  login(loginModel: LoginModel) {
    return this.http.post<UserModel>(LOGIN_URL, loginModel, {observe: 'response'});
  }

  ValidateToken() {
    return this.http.post(VALIDATE_TOKEN_URL, null, {observe: 'response'});
  }
}