import { Injectable } from '@angular/core';
import { GET_CHATS_URL } from './services.constants'
import { LoginModel } from '../../shared/models/login.model'
import { UserModel } from 'src/app/shared/models/user.model';
import { ChatHttpClient } from './chat-http-client';
import { UserMessageModel } from 'src/app/shared/models/user-message.model';

@Injectable()
export class ChatService {
  constructor(private http: ChatHttpClient) { }

  getChats() {
    return this.http.get<Array<UserMessageModel>>(GET_CHATS_URL, {observe: 'response'});
  }
}