export const LOGIN_URL = "/api/users/login";
export const REGISTER_URL = "/api/users/create";
export const VALIDATE_TOKEN_URL = "/api/users/validatetoken";
export const GET_CHATS_URL = "/api/chat/getall";
