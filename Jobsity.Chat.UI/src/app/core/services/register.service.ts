import { Injectable } from '@angular/core';
import { REGISTER_URL } from './services.constants'
import { LoginModel } from '../../shared/models/login.model'
import { UserModel } from 'src/app/shared/models/user.model';
import { ChatHttpClient } from './chat-http-client';

@Injectable()
export class RegisterService {
  constructor(private http: ChatHttpClient) { }

  register(loginModel: LoginModel) {
    return this.http.post<UserModel>(REGISTER_URL, loginModel, {observe: 'response'});
  }
}