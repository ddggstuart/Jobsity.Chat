import { EventEmitter, Injectable } from '@angular/core';
import { HttpTransportType, HubConnection, HubConnectionBuilder } from '@aspnet/signalr';
import { UserMessageModel } from 'src/app/shared/models/user-message.model';
import { UsersManager } from '../managers/users.manager';

@Injectable()
export class SignalRService {
  messageCallback?: (message: UserMessageModel) => void;

  private _hubConnection: HubConnection;  
  
  constructor(usersManager: UsersManager) {  
    this._hubConnection = new HubConnectionBuilder()
      .withUrl(window.location.href + 'api/chathub', {
        accessTokenFactory: ()=> usersManager.getToken(false),
        transport: HttpTransportType.LongPolling
      })
      .build();

    this.registerOnServerEvents();
    this.startConnection();
  }  
  
  sendMessage(message: string) {  
    this._hubConnection.invoke('SendMessage', message);
  }  
  
  private startConnection(): void {  
    this._hubConnection
      .start()  
      .then(() => {  
        console.log('Hub connection started');  
      })  
      .catch(err => {  
        console.log('Error while establishing connection, retrying...');  
        setTimeout(() => this.startConnection(), 5000);
      });  
  }  
  
  private registerOnServerEvents(): void {  
    this._hubConnection.on('ReceiveMessage', (message: UserMessageModel) => {
      if (this.messageCallback)
        this.messageCallback(message)
    });  
  }  
}