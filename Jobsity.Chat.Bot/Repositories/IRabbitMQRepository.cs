﻿namespace Jobsity.Chat.Bot.Repositories
{
    public interface IRabbitMQRepository
    {
        void PublishMessage(string message);
    }
}
