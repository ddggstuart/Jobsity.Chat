﻿using Jobsity.Chat.Bot.Models;
using System.Threading.Tasks;

namespace Jobsity.Chat.Bot.Repositories
{
    public interface IStooqRepository
    {
        Task<StockData> GetStockDataByCodeAsync(string stockCode);
    }
}
