﻿using Jobsity.Chat.Bot.Models;
using Microsoft.Extensions.Options;
using RabbitMQ.Client;
using System;
using System.Text;

namespace Jobsity.Chat.Bot.Repositories
{
    public class RabbitMQApiRepository : IRabbitMQRepository
    {
        private readonly AmqpInfo _amqpInfo;
        private readonly ConnectionFactory _connectionFactory;
        private const string QueueName = "JobsityChatQueue";

        public RabbitMQApiRepository(IOptions<AmqpInfo> ampOptionsSnapshot)
        {
            _amqpInfo = ampOptionsSnapshot.Value;

            _connectionFactory = new ConnectionFactory
            {
                UserName = _amqpInfo.Username,
                Password = _amqpInfo.Password,
                VirtualHost = _amqpInfo.VirtualHost,
                HostName = _amqpInfo.HostName,
                Uri = new Uri(_amqpInfo.Uri)
            };
        }

        public void PublishMessage(string message)
        {
            using (var conn = _connectionFactory.CreateConnection())
            {
                using (var channel = conn.CreateModel())
                {
                    channel.QueueDeclare(
                        queue: QueueName,
                        durable: false,
                        exclusive: false,
                        autoDelete: false,
                        arguments: null
                    );

                    var body = Encoding.UTF8.GetBytes(message);
                    channel.BasicPublish(exchange: "",
                        routingKey: QueueName,
                        basicProperties: null,
                        body: body
                    );
                }
            }
        }
    }
}
