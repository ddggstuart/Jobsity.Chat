﻿using Jobsity.Chat.Bot.Exceptions;
using Jobsity.Chat.Bot.Models;
using RestSharp;
using System;
using System.Globalization;
using System.Threading.Tasks;

namespace Jobsity.Chat.Bot.Repositories
{
    internal class StooqApiRepository : IStooqRepository
    {
        private const string StooqUrl = "https://stooq.com/q/l/?s={0}&f=sd2t2ohlcv&h&e=csv";
        public async Task<StockData> GetStockDataByCodeAsync(string stockCode)
        {
            var client = new RestClient(string.Format(StooqUrl, stockCode))
            {
                Timeout = -1
            };
            var request = new RestRequest(Method.GET);
            IRestResponse response = await client.ExecuteAsync(request);
            return response.IsSuccessful ? MapCsvToStockData(response.Content) : throw new UnavailableStockException();
        }

        private StockData MapCsvToStockData(string plainData)
        {
            var lines = plainData.Split(
                new[] { "\r\n", "\r", "\n" },
                StringSplitOptions.None
            );
            var values = lines[1].Split(",");
            return new StockData()
            {
                Symbol = values[0],
                DateTime = DateTime.ParseExact($"{values[1]} {values[2]}", "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture),
                Open = decimal.Parse(values[3]),
                High = decimal.Parse(values[4]),
                Low = decimal.Parse(values[5]),
                Close = decimal.Parse(values[6]),
                Volume = decimal.Parse(values[7])
            };
        }
    }
}
