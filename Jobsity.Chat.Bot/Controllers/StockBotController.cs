﻿using Jobsity.Chat.Bot.Managers;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Jobsity.Chat.Bot.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class StockBotController : ControllerBase
    {
        private readonly IStocksManager _stocksManager;
        public StockBotController(IStocksManager stocksManager)
        {
            _stocksManager = stocksManager;
        }

        [HttpPost("find/{stockCode}")]
        public async Task<IActionResult> FindStockInformation(string stockCode)
        {
            await _stocksManager.FindStockDataAsync(stockCode);
            return Ok();
        }
    }
}
