﻿using System;

namespace Jobsity.Chat.Bot.Models
{
    public class StockData
    {
        public string Symbol { get; set; }
        public DateTime DateTime { get; set; }
        public decimal Open { get; set; }
        public decimal High { get; set; }
        public decimal Low { get; set; }
        public decimal Close { get; set; }
        public decimal Volume { get; set; }
    }
}
