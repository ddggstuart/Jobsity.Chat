﻿using System.Threading.Tasks;

namespace Jobsity.Chat.Bot.Managers
{
    public interface IStocksManager
    {
        Task FindStockDataAsync(string StockCode);
    }
}
