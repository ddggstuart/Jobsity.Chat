﻿using System;
using Jobsity.Chat.Bot.Repositories;
using System.Threading.Tasks;

namespace Jobsity.Chat.Bot.Managers
{
    public class StocksManager : IStocksManager
    {
        private readonly IStooqRepository _stooqRepository;
        private readonly IRabbitMQRepository _rabbitMQRepository;

        private const string StockMessgeFormat = "{0} quote is {1} per share.";

        public StocksManager(IStooqRepository stooqRepository, IRabbitMQRepository rabbitMQRepository)
        {
            _stooqRepository = stooqRepository ?? throw new ArgumentNullException(nameof(stooqRepository));
            _rabbitMQRepository = rabbitMQRepository ?? throw new ArgumentNullException(nameof(rabbitMQRepository));
        }

        public async Task FindStockDataAsync(string StockCode)
        {
            var stockData = await _stooqRepository.GetStockDataByCodeAsync(StockCode);
            var stockMessage = string.Format(StockMessgeFormat, stockData.Symbol, stockData.Close);
            _rabbitMQRepository.PublishMessage(stockMessage);
        }
    }
}
