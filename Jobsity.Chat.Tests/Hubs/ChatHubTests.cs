﻿using Jobsity.Chat.Hubs;
using Jobsity.Chat.Managers;
using Jobsity.Chat.Models;
using Jobsity.Chat.Repositories;
using Microsoft.AspNetCore.SignalR;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace Jobsit.Chat.Tests.Hubs
{
    public class ChatHubTests
    {
        private readonly Mock<IChatManager> _chatManagerMock = new Mock<IChatManager>();
        private readonly Mock<IStockRepository> _stockRepositoryMock = new Mock<IStockRepository>();
        private readonly Mock<HubCallerContext> _hubCallerContext = new Mock<HubCallerContext>();
        private readonly Mock<IHubCallerClients> _hubCallerClients = new Mock<IHubCallerClients>();
        private readonly Mock<IClientProxy> _clientProxy = new Mock<IClientProxy>();

        [Fact]
        public async Task SendMessage_NotCommand_SaveMessage()
        {
            const string notCommandMessage = "Hi my good old friend";
            var chatHub = new ChatHub(_chatManagerMock.Object, _stockRepositoryMock.Object);
            _hubCallerContext.Setup(e => e.User).Returns(new System.Security.Claims.ClaimsPrincipal());
            _hubCallerClients.Setup(e => e.All).Returns(_clientProxy.Object);
            chatHub.Context = _hubCallerContext.Object;
            chatHub.Clients = _hubCallerClients.Object;
            await chatHub.SendMessage(notCommandMessage);
            _chatManagerMock.Verify(e => e.SaveMessageAsync(It.IsAny<User>(), It.IsAny<string>(), It.IsAny<DateTime>(), It.IsAny<int?>()), Times.Once);
            _stockRepositoryMock.Verify(e => e.SendStockDataRequestAsync(It.IsAny<string>()), Times.Never);
        }

        [Fact]
        public async Task SendMessage_Command_ExecuteCommand()
        {
            const string notCommandMessage = "/stock=aapl.us";
            var chatHub = new ChatHub(_chatManagerMock.Object, _stockRepositoryMock.Object);
            _hubCallerContext.Setup(e => e.User).Returns(new System.Security.Claims.ClaimsPrincipal());
            _hubCallerClients.Setup(e => e.All).Returns(_clientProxy.Object);
            chatHub.Context = _hubCallerContext.Object;
            chatHub.Clients = _hubCallerClients.Object;
            await chatHub.SendMessage(notCommandMessage);
            _chatManagerMock.Verify(e => e.SaveMessageAsync(It.IsAny<User>(), It.IsAny<string>(), It.IsAny<DateTime>(), It.IsAny<int?>()), Times.Never);
            _stockRepositoryMock.Verify(e => e.SendStockDataRequestAsync(It.IsAny<string>()), Times.Once);
        }

        public static IEnumerable<object[]> CommandMessageTestData
        => new object[][] {
                    new object[] { "/stock=aapl.us", "aapl.us" },
                    new object[] { "/stock=appn.us", "appn.us" },
                    new object[] { "Hi mi friend", null },
                    new object[] { "How are you doing", null },
        };
        [Theory]
        [MemberData(nameof(CommandMessageTestData))]
        public void GetCommandMessage_Tests(string command, string expected)
        {
            var chatHub = new ChatHub(_chatManagerMock.Object, _stockRepositoryMock.Object);
            var result = chatHub.GetCommandMessage(command);
            Assert.Equal(expected, result);
        }
    }
}
