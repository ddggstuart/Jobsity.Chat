﻿namespace Jobsity.Chat.Config
{
    internal static class Constants
    {
        public const string DefaultChatRoomName = "Default";
        public const string ReceiveMessageEventKey = "ReceiveMessage";
        public const string BotDefaultName = "Stock Bot";
        public const int MaxChatMessages = 50;
    }
}
