﻿using Jobsity.Chat.Models;
using System.Threading.Tasks;

namespace Jobsity.Chat.Managers
{
    public interface IUsersManager
    {
        Task<UserLogin> CreateUserAsync(string username, string password);
        Task<UserLogin> LoginAsync(string username, string password);
    }
}
