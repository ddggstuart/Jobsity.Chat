﻿using Jobsity.Chat.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Jobsity.Chat.Managers
{
    public interface IChatManager
    {
        Task<IEnumerable<Message>> GetChatRoomMessagesAsync(int? chatRoomId);
        Task SaveMessageAsync(User user, string message, DateTime date, int? chatRoomId);
    }
}
