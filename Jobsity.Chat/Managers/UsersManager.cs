﻿using Jobsity.Chat.Config;
using Jobsity.Chat.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Jobsity.Chat.Managers
{
    public class UsersManager : IUsersManager
    {
        private readonly UserManager<User> _userManager;
        private readonly IConfiguration _config;

        public UsersManager(UserManager<User> userManager, IConfiguration config)
        {
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
            _config = config ?? throw new ArgumentNullException(nameof(config));
        }

        public async Task<UserLogin> CreateUserAsync(string username, string password)
        {
            var result = await _userManager.CreateAsync(new User() 
            { 
                UserName = username 
            }, password);

            if (result.Succeeded)
            {
                return await LoginAsync(username, password);
            }
            else
            {
                return new UserLogin()
                { 
                    Errors = result.Errors.Select(e =>e.Description)
                };
            }
        }

        public async Task<UserLogin> LoginAsync(string username, string password)
        {
            var user = await _userManager.FindByNameAsync(username);
            var result = await _userManager.CheckPasswordAsync(user, password);

            if (result)
            {
                return new UserLogin()
                {
                    Username = user.UserName,
                    JsonWebToken = GenerateJWTToken(user)
                };
            }
            else
            {
                return new UserLogin()
                {
                    Errors = new []
                    {
                        Messages.InvalidLoginAttempt
                    }
                };
            }
        }

        private string GenerateJWTToken(User userInfo)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:SecretKey"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, userInfo.Id),
                new Claim(JwtRegisteredClaimNames.GivenName, userInfo.UserName),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            };
            var token = new JwtSecurityToken(
                issuer: _config["Jwt:Issuer"],
                audience: _config["Jwt:Audience"],
                claims: claims,
                expires: DateTime.Now.AddMinutes(30),
                signingCredentials: credentials
            );
            return new JwtSecurityTokenHandler().WriteToken(token);
        }

    }
}
