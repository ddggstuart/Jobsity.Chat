﻿using Jobsity.Chat.Config;
using Jobsity.Chat.Models;
using Jobsity.Chat.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jobsity.Chat.Managers
{
    public class ChatManager : IChatManager
    {
        private readonly ChatDbContext _chatDbContext;
        public ChatManager(ChatDbContext chatDbContext)
        {
            _chatDbContext = chatDbContext ?? throw new ArgumentNullException(nameof(chatDbContext));
        }

        public async Task<IEnumerable<Message>> GetChatRoomMessagesAsync(int? chatRoomId)
        {
            var chatRoom = await GetChatRoomAsync(chatRoomId);
            return chatRoom.Messages?.OrderByDescending(o => o.Date)?.Take(Constants.MaxChatMessages).Reverse();
        }

        public async Task SaveMessageAsync(User user, string message, DateTime date, int? chatRoomId)
        {
            var chatRoom = await GetChatRoomAsync(chatRoomId);
            await _chatDbContext.Messages.AddAsync(new Message()
            {
                ChatRoomId = chatRoom.Id,
                MessageText = message,
                Date = date,
                UserId = user.Id
            });
            await _chatDbContext.SaveChangesAsync();
        }

        private async Task<ChatRoom> GetDefaultChatRoomAsync()
        {
            var chatRoom = await _chatDbContext.ChatRooms
                .Include(i => i.Messages)
                .ThenInclude(i=>i.User)
                .FirstOrDefaultAsync(e => e.Name == Constants.DefaultChatRoomName);
            if (chatRoom != null)
                return chatRoom;

            chatRoom = new ChatRoom()
            {
                Name = Constants.DefaultChatRoomName
            };
            await _chatDbContext.ChatRooms.AddAsync(chatRoom);
            await _chatDbContext.SaveChangesAsync();
            return chatRoom;
        }

        private async Task<ChatRoom> GetChatRoomAsync(int? chatRoomId)
        {
            if (chatRoomId != null)
                return await _chatDbContext.ChatRooms
                    .Include(i => i.Messages)
                    .ThenInclude(i=> i.User)
                    .FirstOrDefaultAsync(e => e.Id == chatRoomId);
            
            return await GetDefaultChatRoomAsync();
        }
    }
}
