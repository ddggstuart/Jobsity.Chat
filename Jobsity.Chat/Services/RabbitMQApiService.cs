﻿using Jobsity.Chat.Controllers.Models;
using Jobsity.Chat.Hubs;
using Jobsity.Chat.Models;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Jobsity.Chat.Services
{
    public class RabbitMQApiService : BackgroundService
    {
        private readonly IHubContext<ChatHub> _hubContext;
        private readonly AmqpInfo _amqpInfo;
        private ConnectionFactory _connectionFactory;
        private const string QueueName = "JobsityChatQueue";
        private IConnection _connection;
        private IModel _channel;

        public RabbitMQApiService(IOptions<AmqpInfo> ampOptionsSnapshot, IHubContext<ChatHub> hubContext)
        {
            _amqpInfo = ampOptionsSnapshot?.Value ?? throw new ArgumentNullException(nameof(ampOptionsSnapshot));
            _hubContext = hubContext ?? throw new ArgumentNullException(nameof(hubContext));
        }

        public override Task StartAsync(CancellationToken cancellationToken)
        {
            _connectionFactory = new ConnectionFactory
            {
                UserName = _amqpInfo.Username,
                Password = _amqpInfo.Password,
                VirtualHost = _amqpInfo.VirtualHost,
                HostName = _amqpInfo.HostName,
                Uri = new Uri(_amqpInfo.Uri),
                DispatchConsumersAsync = true
            };
            _connection = _connectionFactory.CreateConnection();
            _channel = _connection.CreateModel();
            _channel.QueueDeclarePassive(QueueName);
            _channel.BasicQos(0, 1, false);

            return base.StartAsync(cancellationToken);
        }

        public override async Task StopAsync(CancellationToken cancellationToken)
        {
            await base.StopAsync(cancellationToken);
            _connection.Close();
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            stoppingToken.ThrowIfCancellationRequested();

            var consumer = new AsyncEventingBasicConsumer(_channel);
            consumer.Received += async (bc, ea) =>
            {
                var message = Encoding.UTF8.GetString(ea.Body.ToArray());
                var userMessage = new UserMessageResponse()
                {
                    Message = message,
                    UserName = Config.Constants.BotDefaultName,
                    Date = DateTime.Now
                };
                await _hubContext.Clients.All.SendAsync(Config.Constants.ReceiveMessageEventKey, userMessage);
                _channel.BasicAck(ea.DeliveryTag, false);
            };

            _channel.BasicConsume(queue: QueueName, autoAck: false, consumer: consumer);

            await Task.CompletedTask;
        }
    }
}
