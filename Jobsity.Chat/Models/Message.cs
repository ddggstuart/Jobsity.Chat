﻿using System;

namespace Jobsity.Chat.Models
{
    public class Message
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public int ChatRoomId { get; set; }
        public string MessageText { get; set; }
        public DateTime Date { get; set; }
        public ChatRoom ChatRoom { get; set; }
        public User User { get; set; }
    }
}
