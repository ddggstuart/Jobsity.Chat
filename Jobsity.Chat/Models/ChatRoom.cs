﻿using System.Collections.Generic;

namespace Jobsity.Chat.Models
{
    public class ChatRoom
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<Message> Messages { get; set; }
    }
}
