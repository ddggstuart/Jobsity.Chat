﻿using System.Collections.Generic;

namespace Jobsity.Chat.Models
{
    public class UserLogin
    {
        public string Username { get; set; }
        public string JsonWebToken { get; set; }
        public IEnumerable<string> Errors { get; set; }
    }
}
