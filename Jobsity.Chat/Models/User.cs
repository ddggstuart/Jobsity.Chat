﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Security.Claims;

namespace Jobsity.Chat.Models
{
    public class User : IdentityUser
    {
        public User()
        {
        }

        public User(ClaimsPrincipal user)
        {
            if (user == null)
                throw new ArgumentNullException(nameof(user));

            Id = user.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            UserName = user.FindFirst(ClaimTypes.GivenName)?.Value;
        }

        public static implicit operator User(ClaimsPrincipal user) => new User(user);
    }
}
