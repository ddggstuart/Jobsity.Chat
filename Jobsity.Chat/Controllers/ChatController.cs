﻿using Jobsity.Chat.Controllers.Models;
using Jobsity.Chat.Managers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Jobsity.Chat.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class ChatController : ControllerBase
    {
        private readonly IChatManager _chatManager;
        public ChatController(IChatManager chatManager)
        {
            _chatManager = chatManager ?? throw new ArgumentNullException(nameof(chatManager));
        }

        [HttpGet("getall")]
        public async Task<ActionResult<IEnumerable<UserMessageResponse>>> CreateUser()
        {
            var messages = await _chatManager.GetChatRoomMessagesAsync(null);
            return Ok(UserMessageResponse.Convert(messages));
        }
    }
}
