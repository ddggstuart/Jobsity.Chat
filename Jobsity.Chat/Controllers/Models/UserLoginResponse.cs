﻿using System.Collections.Generic;

namespace Jobsity.Chat.Controllers.Models
{
    public class UserLoginResponse
    {
        public string Username { get; set; }
        public string JsonWebToken { get; set; }
        public IEnumerable<string> Errors { get; set; }

        public static UserLoginResponse Convert(Chat.Models.UserLogin userLogin)
        {
            if (userLogin == null)
                return null;

            return new UserLoginResponse()
            {
                Username = userLogin.Username,
                JsonWebToken = userLogin.JsonWebToken,
                Errors = userLogin.Errors
            };
        }
    }
}
