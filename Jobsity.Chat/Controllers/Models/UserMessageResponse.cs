﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Jobsity.Chat.Controllers.Models
{
    public class UserMessageResponse
    {
        public string UserName { get; set; }
        public string Message { get; set; }
        public DateTime Date { get; set; }

        public static UserMessageResponse Convert(Chat.Models.Message message)
        {
            if (message == null)
                return null;

            return new UserMessageResponse()
            {
                UserName = message.User.UserName,
                Message = message.MessageText,
                Date = message.Date
            };
        }

        public static IEnumerable<UserMessageResponse> Convert(IEnumerable<Chat.Models.Message> message)
        {
            return message?.Select(Convert);
        }
    }
}
