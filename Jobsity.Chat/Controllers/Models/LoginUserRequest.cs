﻿using Jobsity.Chat.Models;

namespace Jobsity.Chat.Controllers.Models
{
    public class LoginUserRequest
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
