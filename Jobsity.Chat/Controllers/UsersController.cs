﻿using Jobsity.Chat.Controllers.Models;
using Jobsity.Chat.Managers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Jobsity.Chat.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly IUsersManager _usersManager;
        public UsersController(IUsersManager usersManager)
        {
            _usersManager = usersManager ?? throw new ArgumentNullException(nameof(usersManager));
        }

        [HttpPost("create")]
        public async Task<ActionResult<UserLoginResponse>> CreateUser(LoginUserRequest loginUserRequest)
        {
            var result = await _usersManager.CreateUserAsync(loginUserRequest.Username, loginUserRequest.Password);
            if (result.Errors == null)
                return Created(result.Username, result);
            else
                return BadRequest(result);
        }

        [HttpPost("login")]
        public async Task<ActionResult<UserLoginResponse>> Login(LoginUserRequest loginUserRequest)
        {
            var result = await _usersManager.LoginAsync(loginUserRequest.Username, loginUserRequest.Password);
            if (result.Errors == null)
                return Ok(result);
            else
                return BadRequest(result);
        }

        [Authorize]
        [HttpPost("validatetoken")]
        public ActionResult ValidateToken()
        {
            return Ok();
        }
    }
}
