﻿using Jobsity.Chat.Config;
using Jobsity.Chat.Controllers.Models;
using Jobsity.Chat.Managers;
using Jobsity.Chat.Models;
using Jobsity.Chat.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Jobsity.Chat.Hubs
{
    [Authorize]
    public class ChatHub : Hub
    {
        private const string CommandRegexValidator = @"^\/stock=(.+)$";
        private readonly IChatManager _chatManager;
        private readonly IStockRepository _stockRepository;

        public ChatHub(IChatManager chatManager, IStockRepository stockRepository)
        {
            _chatManager = chatManager ?? throw new ArgumentNullException(nameof(chatManager));
            _stockRepository = stockRepository ?? throw new ArgumentNullException(nameof(stockRepository));
        }

        public async Task SendMessage(string message)
        {
            var user = (User)Context.User;
            var userMessage = new UserMessageResponse()
            {
                Message = message,
                UserName = user.UserName,
                Date = DateTime.Now
            };
            var command = GetCommandMessage(message);
            if (string.IsNullOrEmpty(command))
                await _chatManager.SaveMessageAsync(user, userMessage.Message, userMessage.Date, null);
            else
                await _stockRepository.SendStockDataRequestAsync(command);

            await Clients.All.SendAsync(Constants.ReceiveMessageEventKey, userMessage);
        }

        internal string GetCommandMessage(string message)
        {
            var match = new Regex(CommandRegexValidator).Match(message);
            if (match.Success)
                return match.Groups[1].Value;

            return null;
        }
    }
}
