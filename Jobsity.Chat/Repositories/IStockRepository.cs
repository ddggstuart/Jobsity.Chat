﻿using System.Threading.Tasks;

namespace Jobsity.Chat.Repositories
{
    public interface IStockRepository
    {
        Task SendStockDataRequestAsync(string stockCode);
    }
}
