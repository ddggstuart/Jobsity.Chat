﻿using Microsoft.Extensions.Configuration;
using RestSharp;
using System;
using System.Threading.Tasks;

namespace Jobsity.Chat.Repositories
{
    public class StockApiRepository : IStockRepository
    {
        private readonly IConfiguration _config;

        public StockApiRepository(IConfiguration config)
        {
            _config = config ?? throw new ArgumentNullException(nameof(config));
        }

        public async Task SendStockDataRequestAsync(string stockCode)
        {
            var client = new RestClient(string.Format(_config["StockServiceUrl"], stockCode))
            {
                Timeout = -1
            };
            var request = new RestRequest(Method.POST);
            var response = await client.ExecuteAsync(request);
        }
    }
}
